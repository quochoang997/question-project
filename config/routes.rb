Rails.application.routes.draw do

  get 'errors/not_found'
  get 'errors/internal_server_error'
  get 'users/list'
  get 'home/index'
  devise_for :users
  get 'tests/index'
  # get 'questions/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  patch  'setStatus'  => 'questions#setStatus'
  post  'create_test' => 'tests#create_test'
  get   'do_test' => 'tests#do_test'
  # put 'do_test' => 'tests#save_test'
  # patch 'do_test' => 'tests#save_test'
  patch  'setRole'  => 'users#setRole'

  get 'all_question' => 'questions#all_question'
  get 'enable_question' => 'questions#enable_question'
  get 'disable_question' => 'questions#disable_question'

  match "/404", :to => "errors#not_found", :via => :all
  match "/500", :to => "errors#internal_server_error", :via => :all

  root 'home#index'
  resources :questions
  resources :tests
  resources :users
end
