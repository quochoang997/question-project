// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require jquery3
//= require popper
//= require bootstrap-sprockets
//= require_tree .
//= _questions.js
//= _tests.js
//= bootstrap4-toggle.min.js


// script for user search
$(document).ready(function() {
	$(".alert" ).fadeOut(3000);

	$("#user_search").on('input', function() {
		$.get($("#user-search").attr("action"), $("#user-search").serialize(), null, "script");
		return false;
	});
});
