$(document).ready(function() {
  
  $(".question-selection").click(function(){
    var parent = $(this).parent().parent().parent();
    if(parent.css("background-color") == "rgb(255, 230, 234)"){
      parent.css("background-color", "white");
      parent.find('.invalid-feedback').css("visibility", "hidden");
    }
    var checked = $(this).find("input").prop('checked') ;
    if(checked == false){
      $(this).css("background-color", "rgba(56, 7, 80, 0.91)");
      $(this).find("input").prop('checked', true);
    }
    else{
      $(this).css("background-color", "#f8f8f8");
      $(this).find("input").prop('checked', false);
    }
  });

  $("#do-test-submit").click(function(event) {
    var i = 1;
    var numChecked ;
		var numItems = $('.question-block').length;
    for (i; i <= numItems; i++) { 
      numChecked = $(".question-index-"+i.toString()+" input:checked").length;
      if(numChecked == 0){
        $(".question-index-"+i.toString()).css("background-color", "#FFE6EA");
				$(".invalid-index-"+i.toString()).css("visibility", "visible");
				event.preventDefault();
      }
    }
		
	});

  // script for test search
	$("#test_search").on('input', function() {
		$.get($("#test-search").attr("action"), $("#test-search").serialize(), null, "script");
		return false;
	});

});