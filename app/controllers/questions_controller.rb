class QuestionsController < ApplicationController
  before_action :only_admin

  def index
    @questions = Question.search(params[:content_search], params[:status_search]).paginate(page: params[:page], per_page: 6)
    # @questions = Question.all
    session[:all] = params[:status_search]
    @total_number_of_question = Question.count
    @number_of_enable_question = Question.count {|question| question.disable == false}
    @number_of_disable_question = Question.count {|question| question.disable == true}
  end
 

  
  def all_question
    @questions = Question.all
    session[:all] = 'all'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def enable_question
    @questions = Question.select {|question| question.disable == false}
    session[:all] = 'enable'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def disable_question
    @questions = Question.select {|question| question.disable == true}
    session[:all] = 'disable'
    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def new
    @question = Question.new
    @question.attach_type = "No attach"
  end

  def show
    @question = Question.find(params[:id])
  end

  def edit
    @question = Question.find(params[:id])
  end

  def create
    @question = Question.new(question_params)
    @question.disable = false
    
    case @question.attach_type
    when "Images"
      @question.image.attach(params[:question][:image])
    when "Sound"
      @question.sound.attach(params[:question][:sound])
    when "Video"
      @question.video.attach(params[:question][:video])
    end
    
    if @question.save
      redirect_to @question
    else
      render 'new'
    end
  end

  def update
    @question = Question.find(params[:id])
    @question.update(disable: true)

    @new_question = Question.new(question_params)
    @new_question.disable = false

    case @new_question.attach_type
    when "Images"
      @new_question.image.attach(params[:question][:image])
    when "Sound"
      @new_question.sound.attach(params[:question][:sound])
    when "Video"
      @new_question.video.attach(params[:question][:video])
    end

    if @new_question.save
      redirect_to @new_question
    else
      render 'edit'
    end
  end

  def setStatus
    @question = Question.find(params[:id])
    if @question.disable == true
      @question.update(disable: false)
      @status = 'Công khai'
      @b = 'Disable'
    else
      @question.update(disable: true)
      @status = 'Vô hiệu hóa'
      @b = 'Enable'
    end

    @number_of_enable_question = Question.count {|question| question.disable == false}
    @number_of_disable_question = Question.count {|question| question.disable == true}

    respond_to do |format|
      format.html {redirect_to questions_path}
      format.js {}
    end
  end

  def question_params
    params.require(:question).permit(:content, :attach_type, :answer1, :answer2, 
      :answer3, :answer4, :correct1, :correct2, :correct3, :correct4)
  end
end
