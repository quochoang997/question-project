class HomeController < ApplicationController
  def index
    if current_user.role == 'user'
      @tests = current_user.tests.all.paginate(page: params[:page], per_page: 6)
    end
  end
end
