class UsersController < ApplicationController
  before_action :only_admin, only: [:index, :setRole]

  def index
    @users = User.search(params[:user_search]).paginate(page: params[:page], per_page: 6)
  end
  
  def setRole
    @user = User.find(params[:id])
    if @user.role == 'user'
      @user.update(role: 'admin')
    else
      @user.update(role: 'user')
    end

    respond_to do |format|
      format.html {redirect_to users_path}
      format.js {}
    end
  end
end
