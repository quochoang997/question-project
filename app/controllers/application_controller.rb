class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  
  def only_admin
    unless current_user.role == 'admin'
      flash[:danger] = "Use must be admin to access this"
      redirect_to root_path
    end
  end
end
