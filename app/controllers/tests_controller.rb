class TestsController < ApplicationController
  before_action :only_admin, only: [:index]
  
  def index
    @tests = Test.search(params[:test_search]).paginate(page: params[:page], per_page: 6)
    # if params[:test_search]
    #   @tests = Test.joins(:user).where(['email LIKE ?', "%#{params[:test_search]}%"])
    # else
    #   @tests = Test.all
    # end
  end

  def create_test
    @questions = Question.select{|question| question.disable == false} #select all enable question
    number_of_question = @questions.count

    @test = Test.create(user: current_user, is_submit: false, score: 0)
    3.times do
      a = true
      begin
        @question = @questions.sample
        unless @test.quizzes.find_by(question: @question)
          a = false
        end
      end while a
      @test.quizzes.build(test: @test, question: @question, check1: false, check2: false, check3: false, check4: false, score: 0)
      
    end
    @test.save
    # redirect_to do_test_path(id: @test.id)
    respond_to do |format|
      format.html {redirect_to do_test_path(id: @test.id)}
      format.js   {render :js => "window.location.href='"+do_test_path(id: @test.id)+"'"}
     end
  end

  def do_test
    @test = Test.find(params[:id])
    p "--------------> Do test"
    if @test.is_submit == true
      p "--------------> Do test : @test.is_submit == true"
      flash[:danger] = 'This test has been submitted'
      redirect_to tests_path
    end
  end

  def update
    @test = Test.find(params[:id])
    @test.update(test_params)
    # @test.update(is_submit: true)

    @quizzes = @test.quizzes
    score = 0
    @quizzes.each do |quiz|
      @question = quiz.question
      if (quiz.check1 == @question.correct1 && quiz.check2 == @question.correct2 && quiz.check3 == @question.correct3 && quiz.check4 == @question.correct4)
        quiz.update(score: 1)
      end
      score += quiz.score
    end
    @test.update(is_submit: true, score: score)
    redirect_to root_path
  end

  def show
    @test = Test.find(params[:id])
    if current_user.role == 'user' and @test.is_submit == false
      flash[:danger] = 'This test has not been submitted'
      redirect_to tests_path
    end
  end

  def test_params
    params.require(:test).permit(quizzes_attributes: [:id, :question, :check1, :check2, :check3, :check4, :score])
  end
end
