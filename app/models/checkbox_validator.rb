class CheckboxValidator < ActiveModel::Validator
    def validate(record)
      if record.correct1 == false and record.correct2 == false and record.correct3 == false and record.correct4 == false
        record.errors[:base] << "One checkbox must be checked"
      end
    end
  end