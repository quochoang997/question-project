class Question < ApplicationRecord
	validates :content, presence: true,
					length: { minimum: 5, maximun: 1000 }
	validates :answer1, :answer2, :answer3, :answer4, presence: true,
					length: { minimum: 1, maximum: 100 }
	validates_with CheckboxValidator
	validate :image_validation, :video_validation, :sound_validation
	
	has_one_attached :image
	has_one_attached :video
	has_one_attached :sound

	def image_validation
		if image.attached?  
			if !image.blob.content_type.starts_with?('image/')
				image.purge
				errors[:base] << 'Attach file is not image'
			elsif image.blob.byte_size > 5242880
				image.purge
				errors[:base] << 'image must be less than 5 MB'
			end
		end
	end

	def video_validation
		if video.attached?  
			if !video.blob.content_type.starts_with?('video/')
				video.purge
				errors[:base] << 'Attach file is not video'
			elsif video.blob.byte_size > 104857600
				video.purge
				errors[:base] << 'video must be less than 100 MB'
			end
		end
	end

	def sound_validation
		if sound.attached?  
			if !sound.blob.content_type.starts_with?('audio/')
				sound.purge
				errors[:base] << 'Attach file is not audio'
			elsif sound.blob.byte_size > 10485760
				sound.purge
				errors[:base] << 'audio must be less than 10 MB'
			end
		end
	end

	def self.search(content_search, status)
		case status
		when 'all'
			if content_search
				where(['content LIKE ?', "%#{content_search}%"])
			else
				all
			end
		when 'enable'
			if content_search
				where(['content LIKE ? AND disable = ?', "%#{content_search}%", false])
			else
				where(['disable = ?', false])
			end
		when 'disable'
			if content_search
				where(['content LIKE ? AND disable = ?', "%#{content_search}%", true])
			else
				where(['disable = ?', true])
			end
		else
			all
		end
	end
end
