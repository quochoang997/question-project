class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :tests
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def self.search(user_search)
    if user_search
      where(['email LIKE ?', "%#{user_search}%"])
    else
      all
    end
  end
end
