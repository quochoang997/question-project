class Test < ApplicationRecord
  belongs_to :user

  has_many :quizzes, :dependent => :destroy
  accepts_nested_attributes_for :quizzes, allow_destroy: true

  def self.search(test_search)
    if test_search
      # Test.where([:user.email.include?(test_search)])
      # Test.select('user.email LIKE ?', "%#{test_search}%")
      # Test.select(:test).joins(:user).where(['email LIKE ?', "%#{test_search}%"]).group("tests.id")
      # Test.joins(:user).where(['email LIKE ?', "%#{test_search}%"])
     
      # Test.where(['score = ?', "%#{test_search}%"])
      # Test.where(['user.email LIKE ?', "%#{test_search}%"])
      # User.where(['email LIKE ?', "%#{test_search}%"])

      Test.joins(:user).where(['email LIKE ?', "%#{test_search}%"])
      # Test.select{|test| test.user.email.include? test_search}
    else
      all
    end
  end
end
