class CreateTests < ActiveRecord::Migration[5.2]
  def change
    create_table :tests do |t|
      t.references :user, foreign_key: true
      t.boolean :is_submit
      t.integer :score

      t.timestamps
    end
  end
end
