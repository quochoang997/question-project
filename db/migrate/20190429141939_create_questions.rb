class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :content
      t.boolean :disable
      t.string :attach_type
      t.string :answer1
      t.string :answer2
      t.string :answer3
      t.string :answer4
      t.boolean :correct1
      t.boolean :correct2
      t.boolean :correct3
      t.boolean :correct4

      t.timestamps
    end
  end
end
