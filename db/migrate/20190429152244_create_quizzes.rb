class CreateQuizzes < ActiveRecord::Migration[5.2]
  def change
    create_table :quizzes do |t|
      t.references :test, foreign_key: true
      t.references :question, foreign_key: true
      t.boolean :check1
      t.boolean :check2
      t.boolean :check3
      t.boolean :check4
      t.integer :score

      t.timestamps
    end
  end
end
